rclone-webdav-container
=======================

> WebDAV server container with rclone and nginx

## Build

The container is built with `nix` and `just`.  See the
[`Justfile`](./Justfile) for all the commands.

```
$ just container
```

The user is defined in [`users.env`](./users.env).  This file needs to
be overwritten when building or in CI with the right values.  For
example, you can do this in the Gitlab `script` or `before_script`
before calling `just container`.  This is circuitous, but we have to
do it like this because Nix flakes don't allow reading environment
variable, not even in `--impure` mode.

## Volumes

The container expects the data dir to be mounted at `/data/`.

## Details

For details, see my [blogpost](https://scvalex.net/posts/70/).
