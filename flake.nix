{
  description = "WebDAV server with rclone and nginx";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages."${system}";
      authConfig = pkgs.runCommand "auth.htpasswd" { } ''
        source ${./users.env}
        ${pkgs.apacheHttpd}/bin/htpasswd -nb "$WEB_USER" "$WEB_PASSWORD" > $out
      '';
      nginxConfig = pkgs.writeText "nginx.conf" ''
        daemon off;
        user nobody nobody;
        error_log /dev/stdout info;
        pid /dev/null;
        events {}
        http {
          server {
            listen 80;
            client_max_body_size 200M;
            location /pub {
              root /data;
              autoindex on;
            }
            location /priv {
              proxy_pass "http://127.0.0.1:8081";
              auth_basic "Files";
              auth_basic_user_file ${authConfig};
            }
            location = / {
              return 301 /pub/;
            }
            location / {
              deny all;
            }
          }
        }
      '';
      startScript = pkgs.writeShellScriptBin "startScript" ''
        set -m
        ${pkgs.rclone}/bin/rclone serve webdav --log-level INFO --baseurl priv --addr 127.0.0.1:8081 /data/ &
        ${pkgs.nginx}/bin/nginx -c ${nginxConfig} &
        fg %1
      '';
    in
    rec {
      defaultPackage = packages.container;

      packages.test = authConfig;

      packages.container = pkgs.dockerTools.buildLayeredImage {
        name = "rclone-webdav";
        tag = "flake";
        created = "now";
        contents = [
          pkgs.fakeNss
        ];
        extraCommands = ''
          # Nginx needs these dirs
          mkdir -p srv/client-temp
          mkdir -p var/log/nginx/
          mkdir -p tmp/nginx_client_body
        '';
        config = {
          ExposedPorts = { "80/tcp" = { }; };
          Entrypoint = [ "${startScript}/bin/startScript" ];
          Cmd = [ ];
        };
      };

      devShell = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          just
          dive
        ];

        GIT_EXTERNAL_DIFF = "${pkgs.difftastic}/bin/difft";
      };
    });
}
