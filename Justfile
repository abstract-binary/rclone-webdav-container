default:
	just --choose

# Build a container
container:
	nix build .#container
	podman load < ./result

# Run the container
up:
	podman container rm rclone-webdav || true
	podman run --rm -it \
	  --name rclone-webdav \
	  --publish 8080:80 \
	  --volume .:/data:rw \
	  "rclone-webdav:flake"

# Inspect the container
dive:
	mkdir -p ~/tmp
	gunzip --stdout ./result > ~/tmp/container.tar
	dive ~/tmp/container.tar --source docker-archive
	rm -f ~/tmp/container.tar
